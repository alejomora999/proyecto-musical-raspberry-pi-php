<?php
    //INICIAR SESION
    session_start();
    if(isset($_SESSION["loggedin"]) &&$_SESSION["loggedin"]===true){
        header("location: plataforma.php");
        exit;
    }
    //inclusion archivo de conexion a BD
    require_once "conexion.php";
    //definicion de variables 
   $email  = $password = $usuario = "";
   $email_err  = $password_err = "";

   if($_SERVER["REQUEST_METHOD"]==="POST"){
        $email_nonespace=trim($_POST["email"]);
        if(empty($email_nonespace)){
            $email_err="Por favor ingrese un email";
        }else{
            $email = $email_nonespace;
        }
        $password_nonespace=trim($_POST["password"]);
        if(empty($password_nonespace)){
            $password_err="Por favor ingrese una contraseña";
        }else{
            $password = $password_nonespace;
        }

        //Validar credenciales
        if (empty($email_err)&&empty($password_err)){
            $sql="SELECT id,usuario,email,clave FROM usuarios WHERE email=?";
            if($stmt=mysqli_prepare($link,$sql)){
                mysqli_stmt_bind_param($stmt, "s",$param_email);
                $param_email=$email;
                if (mysqli_stmt_execute($stmt)){
                    mysqli_stmt_store_result($stmt);
                }
                if(mysqli_stmt_num_rows($stmt)==1){
                    mysqli_stmt_bind_result($stmt, $id, $usuario, $email, $hashed_password);
                    if(mysqli_stmt_fetch($stmt)){
                        if($password==$hashed_password){
                            session_start();

                            //almacenar variables de sesion

                            $_SESSION["loggedin"]=true;
                            $_SESSION["id"]=$id;
                            $_SESSION["email"]=$email;
                            $_SESSION["usuario"]=$usuario;

                            header("location: plataforma.php?usuario=$usuario");
                        }else{
                            $password_err="Contraseña incorrecta";
                        }
                    }
                }
                else{
                    $email_err="El correo no se encuentra registrado";
                }
                
            }
            else{
                echo "Algo salió mal";
            }
        }

        //GENERAR JSON
        //generacion de la consulta
        $sql ="SELECT * FROM canciones";
        mysqli_set_charset($link,"utf8");//formato de datos utf8
        if(!$result=mysqli_query($link,$sql)) {
            die();
        }
        $songs =array();//array de canciones
        while($row=mysqli_fetch_array($result)){
            $id =$row['id'];
            
            $nombre=$row['nombre'];
            $song=$row['song'];
            $song=stripslashes($song);
            $imagen=$row['imagen'];
            $artista=$row['artista'];
            $songs[]=array ('id'=>$id, 'nombre'=> $nombre,'song'=>$song,'imagen'=>$imagen, 'artista'=>$artista);
        }

        
        mysqli_close($link);
        //se crea el json
        $json_string =json_encode($songs);
        //archivo de tipo json
        $file= 'js/app.json';
        file_put_contents($file, $json_string);
   }
    
?>
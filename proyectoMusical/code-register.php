<?php
    //inclusion archivo de conexion a BD
    require_once "conexion.php";
    //definicion de variables 
    $username = $email  = $password = "";

    $username_err = $email_err  = $password_err = "";
    
    if($_SERVER["REQUEST_METHOD"] =="POST"){
        //VALIDACION NOMBRE DE USUARIO
        $username_nospace = trim($_POST["username"]);
       if (empty($username_nospace)) {
           $username_err ="Por favor ingrese un nombre de usuario";
       }
       else {
           //prepara una declaracion de seleccion
           $sql= "SELECT id FROM usuarios WHERE usuario = ?";
           if ($stmt = mysqli_prepare ($link, $sql)){
               mysqli_stmt_bind_param($stmt,"s",$param_username);
               $param_username= trim ($_POST["username"]);
               if (mysqli_stmt_execute($stmt)){
                   mysqli_stmt_store_result($stmt);

                   if(mysqli_stmt_num_rows($stmt)==1){ //valida si ya existe el 
                       $username_err= "Este nombre de usuario ya está registrado";
                   }else{
                       $username= trim($_POST ["username"]);
                   }
               } else{
                   echo "Algo salió mal";
               }
           }
       }
       //VALIDACION EMAIL
       $email_nospace = trim($_POST["email"]);
       if (empty($email_nospace)) {
           $email_err ="Por favor ingrese un email";
       }
       else {
           //prepara una declaracion de seleccion
           $sql= "SELECT id FROM usuarios WHERE email = ?";
           if ($stmt = mysqli_prepare ($link, $sql)){
               mysqli_stmt_bind_param($stmt,"s",$param_email);
               $param_email= trim ($_POST["email"]);
               if (mysqli_stmt_execute($stmt)){
                   mysqli_stmt_store_result($stmt);

                   if(mysqli_stmt_num_rows($stmt)==1){ //valida si ya existe la contraseña 
                       $email_err= "Este email ya está registrado";
                   }else{
                       $email= trim($_POST ["email"]);
                   }
               } else{
                   echo "Algo salió mal";
               }
           }
       }
       //VALIDACION CONTRASEÑA
       $password_nospace = trim($_POST["password"]);
       if (empty($password_nospace)) {
           $password_err ="Por favor ingrese una contraseña";
       }elseif(strlen($password_nospace)<4){
        $password_err ="La contraseña debe de tener al menos 4 caracteres";
       }else{
           $password= $password_nospace;
       }

       //comprobacion de los errores de entrada antes de insertar los datos a la BD

       if (empty ($username_err) && empty($email_err) && empty($password_err)){
        // preparacion de la sentencia
            $sql= "INSERT INTO usuarios (usuario, email, clave) VALUES (?,?,?)";
            if($stmt = mysqli_prepare($link,$sql)){
                mysqli_stmt_bind_param($stmt,"sss",$param_username,$param_email,$param_password);
                

                //establecimiento de parámetros
                $param_username=$username;
                $param_email=$email;
                $param_password= $password;

                if(mysqli_stmt_execute($stmt)){
                    header("location:index.php");
                }else{
                    echo "Algo salió mal";
                }
            }
       }
       mysqli_close($link);
    }

?>

<?php
    include 'code-login.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Plataforma musical PI - Login</title>
    <link rel="stylesheet" href="css/estilos.css"> 
</head>
<body>
    <div class="container-all">
        <div class="ctn-form">
            <img src="images/logo.png" alt="" class="logo">
            <h1 class="title">Iniciar Sesión</h1>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"])?>" method="post">
                <label for="">
                    Email
                </label>
                <input type="text" name="email">
                <span class="msg-error"><?php echo $email_err;?></span>
                <label for="">
                    Contraseña
                </label>
                <input type="password" name="password">
                <span class="msg-error"><?php echo $password_err;?></span>
                <input type="submit" value="Iniciar">
            </form>
            <span class="text-footer">¿Aún no te has registrado? <a href="register.php">Registrate</a></span>

        </div>
        <div class="ctn-text">
            <div class="capa">
                <h1 class="title-description">
                    Acompañamos tus mejores momentos
                </h1>
                <p class="text-description">
                    Normalmente se dice, que si la vida te da limones haz limonada. Nosotros te decimos, danos una frambuesa y te daremos musica, somos una plataforma en proceso de construccion creado por universitarios y para universitarios. Gracias por visitarnos ;D 
                </p>

            </div>
        </div>
    </div>
</body>
</html>

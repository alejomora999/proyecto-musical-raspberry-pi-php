$(document).ready(function(){
    getSongs();
});
var audio;
var music;
function getSongs(){
    $.getJSON("js/app.json", function(mjson){
        music=mjson;
        console.log(music);
        genList(music);
        validarCancion();
    });
}
function playSong(id){
    console.log(id);
    $('#img-album').attr('src',music[id].imagen);
    $('#player').attr('src',music[id].song);
}
function genList(music){
    $.each(music,function(i,song){
        
        $('#playlist').append('<li class="list-group-item" id="'+i+'">'+song.nombre+'</li>');
    });
}
function validarCancion(){
    $('#playlist li').click(function(){
        var selectedsong =$(this).attr('id');
        playSong(selectedsong);
    }); 
}



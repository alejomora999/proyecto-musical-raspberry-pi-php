<?php
session_start();
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"]!== true){
    header("location: index.php");
}
$usuario =$_GET["usuario"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Plataforma Musical PI- Home</title>
    <link rel="stylesheet" href="css/estilos-plataforma.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-3.4.1.min.js"> </script> 
    <script src="js/bootstrap.min.js"></script>
    
</head>
<body>
    <div class="topnav">
            <a class="usuario">Bienvenido <?php echo $usuario; ?></a>
        <a href="cerar-sesion.php" class="close-sesion"> Cerrar sesión</a>
    </div>
   <!--<div class="ctn-buscador">
        <form action="">
            <input type="text" class="buscador" name="buscar" id="filtrar">
            <input type="submit" value="Buscar" id="filtro">
        </form>
    </div>-->
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            <img id="img-album" src="images/logo.png" width="300">
            <audio id="player" controls="" src=""></audio>
            </div>
            <div class="col-md-6">
            <ul class="list-group" id="playlist"></ul>
            </div>
        </div>
    </div>
<script src="js/app.js"></script>
</body>
</html>